package com.example.demo.sys.vo;

import com.example.demo.sys.domain.Job;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JobVO extends Job {
	private String job;
}
