package com.example.demo.sys.vo;

import com.example.demo.sys.domain.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
public class UserVO extends User {

	private String job;	//职位数据
	private String dept;//部门
	private String role;//角色
	private Integer deptId;//部门id
	private String userNo;//用户编号
	private String deptCoNo;//部门编号

}
