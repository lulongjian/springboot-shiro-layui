package com.example.demo.sys.controller;

import com.example.demo.sys.domain.Permission;
import com.example.demo.sys.form.PermissionForm;
import com.example.demo.sys.form.RoleForm;
import com.example.demo.sys.service.PermissionService;
import com.example.demo.sys.utils.JsonModel;
import com.example.demo.sys.vo.PermissionVO;
import com.example.demo.sys.vo.RoleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限管理
 */
@Controller
@RequestMapping("permission")
public class PermissionController {

	@Autowired
	private PermissionService permissionService;

	/**
	 *返回权限表树的json
	 */
	@RequestMapping("permissionList")
	@ResponseBody
	public JsonModel findAll(PermissionForm permissionForm, @RequestParam("roleId") Integer roleId){
		JsonModel jsonModel = new JsonModel();

		//将传过来的参数转化成数组
		List<Permission> pVOS = new ArrayList<>() ;
		if (roleId != 0){
			permissionForm.setRoleId(roleId);
			pVOS = permissionService.findAll(permissionForm);
		}
		//查找全部的职位
		List<Permission> permissionS  = permissionService.findAll(new PermissionForm());
		List<PermissionVO> permissionVOS = new ArrayList<>();
		//封装成树结构
		boolean flag = true;
		for (Permission permission : permissionS){
			String name = permission.getTitle();
			Integer value = permission.getId();

			//遍历查找id是否是有该权限，若是则选中
			for (Permission p : pVOS){
				if(value == p.getId()){
					permissionVOS.add(new PermissionVO(name, value,"selected",""));
					flag = false;
				}
			}
			if(flag){
				permissionVOS.add(new PermissionVO(name, value,"",""));
			}
			flag = true;
		}
		jsonModel.setCode(0);
		jsonModel.setData(permissionVOS);
		return jsonModel;
	}

	/**
	 *返回json列表数据
	 */
	@RequestMapping("permissionListJson")
	@ResponseBody
	public JsonModel permissionListJson(PermissionForm permissionForm){
		JsonModel jsonModel = new JsonModel();
		//查找全部的职位
		List<Permission> permissions  = permissionService.findAll(permissionForm);

		jsonModel.setCode(0);
		jsonModel.setData(permissions);
		return jsonModel;
	}

	/**
	 * 添加权限
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel add(PermissionForm permissionForm, String role){
		JsonModel jsonModel = new JsonModel();

		//先判断权限是否已存在
		List<Permission> permissions = permissionService.findAll(new PermissionForm());
		for (Permission permission : permissions){
			if(permission.getTitle().equals(permissionForm.getTitle())){
				jsonModel.setMsg("失败！该部门已存在请重新添加");
				return jsonModel;
			}
		}
		//添加角色信息
		permissionService.add(permissionForm);
		//绑定权限与角色的关系
		if(!"".equals(role)){
			//不为空才添加
			String[] roleIds = role.split(",");
			for (String roleId : roleIds){
				permissionService.bandRole(permissionForm.getId(), Integer.parseInt(roleId));
			}
		}
		jsonModel.setMsg("添加权限成功！");
		jsonModel.setCode(0);
		return jsonModel;
	}
	/**
	 *删除单个权限
	 */
	@RequestMapping("delete")
	@ResponseBody
	public JsonModel delete(Integer id){
		JsonModel jsonModel = new JsonModel();
		//删除权限
		permissionService.delete(id);
		//解除权限的角色关系
		permissionService.deleteRole(id);
		jsonModel.setCode(0);
		jsonModel.setMsg("删除成功！");
		return jsonModel;
	}
}
