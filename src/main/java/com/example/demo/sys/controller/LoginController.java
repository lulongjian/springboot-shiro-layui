package com.example.demo.sys.controller;

import com.example.demo.sys.domain.User;
import com.example.demo.sys.utils.Constant;
import com.example.demo.sys.utils.JsonModel;
import com.example.demo.sys.utils.RASEncrypt;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("sys")
public class LoginController {

	/**
	 *登陆逻辑处理
	 */
	@RequestMapping(value="login", method= RequestMethod.POST)
	@ResponseBody
	public JsonModel login(String username, String password, Model model, HttpSession session){
		String loginName = username;
		JsonModel jsonModel = new JsonModel();
		System.out.println("登陆");
		if(loginName==null){
			return jsonModel;
		}

		try {
			 password = RASEncrypt.decrypt(password, Constant.PRIVATEKRY);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/**
		 * 使用Shiro编写认证操作
		 */
		//1.获取subject
		Subject subject = SecurityUtils.getSubject();

		//2.封装用户数据
		char[] passwordChar = password.toCharArray();
		UsernamePasswordToken token = new UsernamePasswordToken();
		token.setUsername(loginName);
		token.setPassword(passwordChar);
		//3.执行登陆方法
		try {
			subject.login(token);
			//登陆成功
			//查询该用户的角色
			User user = (User)subject.getPrincipal();
			session.setAttribute("loginName", loginName);
			session.setAttribute("userId", user.getId());

			jsonModel.setCode(0);
			jsonModel.setMsg("成功");
			return jsonModel;
		} catch (UnknownAccountException e) {
			//登陆失败：用户名不存在
			jsonModel.setMsg("用户名不存在!");
			jsonModel.setCode(1);

			return jsonModel;
		} catch (IncorrectCredentialsException e){
			//登陆失败：密码错误
			jsonModel.setMsg("密码错误!");
			return jsonModel;
		}
	}

	/**
	 *判断是否已经存在登陆账号
	 */
	@RequestMapping("login")
	public String toLogin(HttpSession session){

		//判断是否已经存在登陆账号
		if(session.getAttribute("loginName")!=null){
			return "redirect:/sys/index";
		}
		return "sys/login";
	}

	/**
	 *退出登陆逻辑处理
	 */
	@RequestMapping(value="logout", method= RequestMethod.GET)
	public String logout(){
		//1.获取subject
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:/sys/login";
	}

}
