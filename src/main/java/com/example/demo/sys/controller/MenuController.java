package com.example.demo.sys.controller;

import com.example.demo.sys.domain.Menu;
import com.example.demo.sys.service.MenuService;
import com.example.demo.sys.utils.JsonModel;
import com.example.demo.sys.utils.TreeNode;
import com.example.demo.sys.utils.TreeNodeBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("menu")
public class MenuController {

	@Autowired
	private MenuService menuService;

	/**
	 *加载菜单,封装返回json
	 */
	@RequestMapping("menuList")
	@ResponseBody
	public JsonModel menu(HttpSession session){
		JsonModel jsonModel = new JsonModel();

		Integer userId = (Integer) session.getAttribute("userId");
		List<Menu> menusList = menuService.findUserPermissionMenu(userId);

		List<Menu> menus = menuService.findAll();
		List<Menu> menusListJson = new ArrayList<>();
		for(int i = 0; i<menus.size(); i++){
			if(menus.get(i).getPid() ==0){
				menusListJson.add(menus.get(i));
				continue;
			}
			for (Menu menu : menusList){
				if(menus.get(i).getId() == menu.getId()){
					menusListJson.add(menus.get(i));
					continue;
				}
			}
		}
		//封装成树结构
		List<TreeNode> treeNodes = new ArrayList<TreeNode>();
		for (Menu menu : menusListJson) {
			Integer id = menu.getId();
			Integer pid = menu.getPid();
			String title = menu.getTitle();
			String icon = menu.getIcon();
			String href = menu.getHref();
			Boolean spread = menu.getOpen()==1?true:false;
			treeNodes.add(new TreeNode(id, pid, title, icon, href, spread));
		}
		//构造层级关系
		List<TreeNode> list = TreeNodeBuilder.build(treeNodes,0);
		jsonModel.setData(list);
		return jsonModel;
	}

	/**
	 *加载菜单,封装返回json
	 */
	@RequestMapping("loadMenuManagerLeftTreeJson")
	@ResponseBody
	public JsonModel loadMenuManagerLeftTreeJson(){
		JsonModel jsonModel = new JsonModel();
		List<Menu> menus = menuService.findAll();

		//封装成树结构
		List<TreeNode> treeNodes = new ArrayList<TreeNode>();
		for (Menu menu : menus) {
			Integer id = menu.getId();
			Integer pid = menu.getPid();
			String title = menu.getTitle();
			String href = menu.getHref();
			String icon = menu.getIcon();
			Boolean spread = menu.getOpen()==1?true:false;
			treeNodes.add(new TreeNode(id, pid, title, spread));
		}
		jsonModel.setCode(0);
		jsonModel.setData(treeNodes);
		return jsonModel;
	}
}
