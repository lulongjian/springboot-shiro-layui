package com.example.demo.sys.controller;

import com.example.demo.sys.form.JobForm;
import com.example.demo.sys.service.JobService;
import com.example.demo.sys.utils.JsonModel;
import com.example.demo.sys.vo.SelectJsonVO;
import com.example.demo.sys.vo.JobVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 职位管理
 */
@Controller
@RequestMapping("job")
public class JobController {
	@Autowired
	private JobService jobService;


	/**
	 * 查找职位列表多选格式
	 */
	@RequestMapping("jobList")
	@ResponseBody
	public JsonModel findAll(JobForm jobForm, @RequestParam("deptId") String deptId){
		JsonModel jsonModel = new JsonModel();
		//将传过来的参数转化成数组
		jobForm.setDeptId(deptId);
		List<JobVO> jobs = jobService.findAll(jobForm);

		//查找全部的职位
		List<JobVO> jobVOS = jobService.findAll(new JobForm());
		List<SelectJsonVO> jobFormVOS = new ArrayList<>();
		//封装成树结构
		boolean flag = true;
		for (JobVO jobVO : jobVOS){
			String name = jobVO.getTitle();
			Integer value = jobVO.getId();

			//遍历查找id是否是该部门的，若是则选中
			for (JobVO j : jobs){
				if(value == j.getId()){
					jobFormVOS.add(new SelectJsonVO(name, value,"selected",""));
					flag = false;
				}
			}
			if(flag){
				jobFormVOS.add(new SelectJsonVO(name, value,"",""));
			}
			flag = true;
		}
		jsonModel.setCode(0);
		jsonModel.setData(jobFormVOS);
		return jsonModel;
	}
	/**
	 * 查找职位列表单选
	 */
	@RequestMapping("jobLists")
	@ResponseBody
	public JsonModel findAlls(JobForm jobForm){
		JsonModel jsonModel = new JsonModel();

		List<JobVO> jobVOS = jobService.findAll(jobForm);

		jsonModel.setCode(0);
		jsonModel.setData(jobVOS);
		return jsonModel;
	}

	/**
	 * 通过名字查找job
	 */
	@RequestMapping("jobId")
	@ResponseBody
	public JsonModel findByJobName(String title){
		JsonModel jsonModel = new JsonModel();

		JobVO jobVO = jobService.findJobByJobName(title);
		jsonModel.setData(jobVO);
		return jsonModel;
	}
	/**
	 * 添加job
	 */
	@RequestMapping("add")
	@ResponseBody
	public JsonModel add(String title, String remark){
		JsonModel jsonModel = new JsonModel();
		JobForm jobForm = new JobForm();
		//判断该职位是否已存在
		List<JobVO> jobVOS = jobService.findAll(jobForm);
		for(JobVO jobVO : jobVOS){
			if(jobVO.getTitle().equals(title)){
				jsonModel.setCode(1);
				jsonModel.setMsg("添加失败，该职位已存在");
				return jsonModel;
			}
		}
		jobForm.setTitle(title);
		jobForm.setRemark(remark);

		jobService.add(jobForm);
		jsonModel.setCode(0);
		jsonModel.setMsg("添加成功");
		return jsonModel;
	}

	/**
	 *删除单个职位
	 */
	@RequestMapping("delete")
	@ResponseBody
	public JsonModel delete(Integer id){
		JsonModel jsonModel = new JsonModel();
		//先删除与职位与用户的关联
		jobService.deleteBandUser(id);
		//删除职位与部门信息
		jobService.deleteBandDept(id);
		//删除职位信息
		jobService.delete(id);
		jsonModel.setCode(0);
		jsonModel.setMsg("删除成功！");
		return jsonModel;
	}

	/**
	 *修改部门信息
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel update(Integer id, String title, String remark){
		JsonModel jsonModel = new JsonModel();
		JobForm jobForm = new JobForm();
		//判断该职位名称是否已存在
		List<JobVO> jobVOS = jobService.findAll(jobForm);
		for(JobVO jobVO : jobVOS){
			if(jobVO.getTitle().equals(title) && id != jobVO.getId()){
				jsonModel.setCode(1);
				jsonModel.setMsg("修改失败，该职位已存在");
				return jsonModel;
			}
		}
		jobForm.setId(id);
		jobForm.setTitle(title);
		jobForm.setRemark(remark);

		jobService.update(jobForm);

		jsonModel.setCode(0);
		jsonModel.setMsg("更新成功");
		return jsonModel;
	}
}
