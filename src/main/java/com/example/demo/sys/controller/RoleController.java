package com.example.demo.sys.controller;

import com.example.demo.sys.form.RoleForm;
import com.example.demo.sys.service.RoleService;
import com.example.demo.sys.utils.JsonModel;
import com.example.demo.sys.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色管理
 */
@Controller
@RequestMapping("role")
public class RoleController {
	@Autowired
	private RoleService roleService;

	/**
	 *角色列表
	 */
	@RequestMapping("roleList")
	@ResponseBody
	public JsonModel findAll(RoleForm roleForm){
		JsonModel jsonModel = new JsonModel();

		List<RoleVO> roleVOS = roleService.findAll(roleForm);

		jsonModel.setCode(0);
		jsonModel.setData(roleVOS);
		return jsonModel;
	}

	/**
	 *角色select多选列表
	 */
	@RequestMapping("roleListSelectJson")
	@ResponseBody
	public JsonModel roleListSelectJson(RoleForm roleForm, @RequestParam("permissionId") Integer permissionId){
		JsonModel jsonModel = new JsonModel();

		List<RoleVO> vos = new ArrayList<>();
		if (permissionId != 0){
			roleForm.setPermissionId(permissionId);
			vos= roleService.findAll(roleForm);
		}
		List<RoleVO> roleVOS = roleService.findAll(new RoleForm());

		List<SelectJsonVO> roleVOSList = new ArrayList<>();
		boolean flag = true;
		for (RoleVO roleVO : roleVOS){
			String name = roleVO.getTitle();
			Integer value = roleVO.getId();

			//遍历查找id是否是该部门的，若是则选中
			for (RoleVO v : vos){
				if(value == v.getId()){
					roleVOSList.add(new SelectJsonVO(name, value,"selected",""));
					flag = false;
				}
			}
			if(flag){
				roleVOSList.add(new SelectJsonVO(name, value,"",""));
			}
			flag = true;
		}

		jsonModel.setCode(0);
		jsonModel.setData(roleVOSList);
		return jsonModel;
	}

	/**
	 * 添加角色
	 */
	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel add(RoleForm roleForm,String permission){
		JsonModel jsonModel = new JsonModel();

		if("".equals(permission)){

		}
		//先判断角色是否已存在
		List<RoleVO> roleVOS = roleService.findAll(new RoleForm());
		for (RoleVO roleVO:roleVOS){
			if(roleVO.getTitle().equals(roleForm.getTitle())){
				jsonModel.setMsg("失败！该角色已存在请重新添加");
				return jsonModel;
			}
		}
		//添加角色信息
		 roleService.add(roleForm);
		//绑定权限
		if(!"".equals(permission)){
			//不为空才添加
			String[] permissionIds = permission.split(",");
			for (String permissionId : permissionIds){
				roleService.bandPermission(roleForm.getId(),Integer.parseInt(permissionId));
			}
		}
		jsonModel.setMsg("添加角色成功！");
		jsonModel.setCode(0);
		return jsonModel;
	}

	/**
	 *删除单个角色
	 */
	@RequestMapping("delete")
	@ResponseBody
	public JsonModel delete(Integer id){
		JsonModel jsonModel = new JsonModel();
		//解除角色与员工
		roleService.deleteBandUser(id);
		//解除角色的权限
		roleService.deletePermission(id);
		//删除角色信息
		roleService.delete(id);
		jsonModel.setCode(0);
		jsonModel.setMsg("删除成功！");
		return jsonModel;
	}

	/**
	 *修改角色
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public JsonModel update(RoleForm roleForm,String permission){
		JsonModel jsonModel = new JsonModel();

		if("".equals(permission)){

		}
		//先判断角色是否已存在
		List<RoleVO> roleVOS = roleService.findAll(new RoleForm());
		for (RoleVO roleVO:roleVOS){
			if(roleVO.getTitle().equals(roleForm.getTitle()) && roleVO.getId() != roleForm.getId()){
				jsonModel.setMsg("失败！该部门已存在请重新编辑");
				return jsonModel;
			}
		}
		//解绑关系
		roleService.deletePermission(roleForm.getId());
		//修改角色信息
		roleService.update(roleForm);
		//绑定权限
		if(!"".equals(permission)){
			//不为空才添加
			String[] permissionIds = permission.split(",");
			for (String permissionId : permissionIds){
				roleService.bandPermission(roleForm.getId(),Integer.parseInt(permissionId));
			}
		}
		jsonModel.setMsg("修改角色成功！");
		jsonModel.setCode(0);
		return jsonModel;
	}
}
