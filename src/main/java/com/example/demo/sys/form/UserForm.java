package com.example.demo.sys.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 封装前端传进来的数据
 */
@Data
@NoArgsConstructor
public class UserForm {

	private Integer id;
	private String userNo;
	private String name; //封装查询用户名
	private String loginName;
	private String password;
	private Integer deptId;
	private Integer role;
	private Integer jobId;
	private String job;
	private String deptCoNo;//部门编号
	private String email; //封装前端查询的邮箱
	private String phone;
	private Integer sex;
	private Date createTime; //添加时间
	private Date updateTime; //更新时间


}
