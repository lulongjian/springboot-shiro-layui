package com.example.demo.sys.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
public class User {

	private Integer id;
    private String name;
    private String loginName;
    private String password;
	private String phone;
	private String email;
	private String userNo;
	private Integer sex;
	private Integer deptNo;
	private String deptCoNo;//部门编号
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime; //添加时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime; //更新时间

}
