package com.example.demo.sys.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
public class Dept {

	private Integer id;
	private String deptNo;//部门编号
	private Integer pid;	//上级部门id
	private String parentDeptNo;//上级部门编号
	private String parentDept;//上级部门
	private String title;	//部门名字
	private Integer open;	//0代表可展开 1代表不可展开
	private String address;	//地区
	private String remark;	//描述
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime; //添加时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime; //更新时间

}
