package com.example.demo.sys.service;

import com.example.demo.sys.domain.Menu;

import java.util.List;

public interface MenuService {

	List<Menu> findAll();	//查找所有菜单

	List<Menu> findUserPermissionMenu(Integer userId); //查找用户对应的菜单
}
