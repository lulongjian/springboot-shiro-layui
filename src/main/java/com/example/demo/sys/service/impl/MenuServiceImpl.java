package com.example.demo.sys.service.impl;



import com.example.demo.sys.dao.MenuMapper;
import com.example.demo.sys.domain.Menu;
import com.example.demo.sys.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuMapper menuMapper;

	/**
	 * 查找所有菜单
	 */
	@Override
	public List<Menu> findAll() {
		return menuMapper.findAll();
	}

	@Override
	public List<Menu> findUserPermissionMenu(Integer userId) {
		return menuMapper.findUserPermissionMenu(userId);
	}

}
