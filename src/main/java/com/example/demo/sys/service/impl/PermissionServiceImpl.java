package com.example.demo.sys.service.impl;

import com.example.demo.sys.dao.PermissionMapper;
import com.example.demo.sys.domain.Permission;
import com.example.demo.sys.form.PermissionForm;
import com.example.demo.sys.service.PermissionService;
import com.example.demo.sys.vo.PermissionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private PermissionMapper permissionMapper;


	@Override
	public List<Permission> findAll(PermissionForm permissionForm) {
		return permissionMapper.findAll(permissionForm);
	}

	@Override
	public int add(PermissionForm permissionForm) {
		permissionForm.setCreateTime(new Date());
		permissionForm.setUpdateTime(new Date());
		return permissionMapper.add(permissionForm);
	}

	@Override
	public int bandRole(Integer permisssionId, Integer roleId) {
		return permissionMapper.bandRole(permisssionId, roleId);
	}

	@Override
	public int delete(Integer id) {
		return permissionMapper.delete(id);
	}

	@Override
	public int deleteRole(Integer permissionId) {
		return permissionMapper.deleteRole(permissionId);
	}


}
