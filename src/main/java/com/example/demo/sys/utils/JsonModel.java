package com.example.demo.sys.utils;

public class JsonModel {
	private Integer code;	//状态码
    private String msg;
    private Object data;
    private Long count;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "JsonModel{" +
				"code=" + code +
				", msg='" + msg + '\'' +
				", data=" + data +
				", count=" + count +
				'}';
	}
}
