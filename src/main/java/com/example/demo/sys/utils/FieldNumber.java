package com.example.demo.sys.utils;

public class FieldNumber {
	//生成员工编号，不足补0
	public static String number(Integer num){
		String str = String.format("%4d", num+1).replace(" ", "0");
		return str;
	}
}
