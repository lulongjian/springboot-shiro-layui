package com.example.demo.sys.dao;

import com.example.demo.sys.form.JobForm;
import com.example.demo.sys.vo.JobVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface JobMapper {

	List<JobVO> findAll(JobForm jobForm);//查找所有的角色信息

	JobVO findJobByJobName(String title);//根据职位名字查找id

	int add(JobForm jobForm);//添加职位

	int deleteBandUser(Integer jobId);//删除职位与用户绑定关系

	int deleteBandDept(Integer jobId);//删除与部门的绑定关系

	int delete(Integer id);//删除用户信息

	int update(JobForm jobForm);//更新用户信息
}
