package com.example.demo.sys.dao;


import com.example.demo.sys.domain.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper {

	List<Menu> findAll();	//查找所有菜单

	List<Menu> findUserPermissionMenu(Integer userId); //查找用户对应的菜单
}
