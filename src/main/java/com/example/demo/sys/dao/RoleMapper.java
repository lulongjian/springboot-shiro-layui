package com.example.demo.sys.dao;

import com.example.demo.sys.form.RoleForm;
import com.example.demo.sys.vo.RoleVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleMapper {

	List<RoleVO> findAll(RoleForm roleForm);//查找所有的角色信息

	int add(RoleForm roleForm); //添加角色

	int bandPermission(@Param("roleId") int roleId, @Param("permissionId")Integer permissionId); //绑定权限

	int delete(Integer id);//删除角色

	int deletePermission(Integer id);//删除角色的权限关系

	int deleteBandUser(Integer id);//删除角色的用户关系

	int update(RoleForm roleForm);//修改角色信息
}
