package com.example.demo.sys.dao;

import com.example.demo.sys.domain.User;
import com.example.demo.sys.form.DeptForm;
import com.example.demo.sys.form.UserForm;
import com.example.demo.sys.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {

    User findByloginName(String loginName);//通过用户名字查找,验证登陆

	List<UserVO> findAll(UserForm userForm); //查找用户列表

	int delete(Integer id);	//删除用户

	int deleteUserJob(Integer userId); //删除与job中间的关联表

	int deleteUserRole(Integer userId); //删除与job中间的关联表

	List<User> findNameOrLoginName(@Param("name") String name, @Param("loginName") String loginName);//通过用户姓名和登陆名字查找,验证添加是否重复

	int add(UserForm userForm);//添加用户的信息

	int bandUserJob(UserForm userForm);//绑定用户的职位关系

	int bandUserRole(UserForm userForm);//绑定用户的角色关系

	int update(UserForm userForm);	//更新用户信息

	int updateDeptId(Integer deptId);//更新关系部门，删除部门是解绑关系

	List<String> findPermissionByUserId(Integer id);//查找相应的权限
}
